use relm4::{
    actions::{ActionGroupName, RelmAction, RelmActionGroup},
    adw, gtk, main_application, Component, ComponentController, ComponentParts, ComponentSender,
    Controller, SimpleComponent,
};

use adw::prelude::*;
use gtk::prelude::{ApplicationExt, ApplicationWindowExt, GtkWindowExt, SettingsExt, WidgetExt};
use gtk::{gio, glib};

use crate::config::{APP_ID, PROFILE};
use crate::modals::about::AboutDialog;

use crate::pages::{contacts::Contacts, files::Files, photos::Photos};

pub struct App {
    about_dialog: Controller<AboutDialog>,
    contacts: Controller<Contacts>,
    photos: Controller<Photos>,
    files: Controller<Files>,
}

#[derive(Debug)]
pub enum AppMsg {
    Ok,
    Quit,
}

relm4::new_action_group!(pub(super) WindowActionGroup, "win");
relm4::new_stateless_action!(PreferencesAction, WindowActionGroup, "preferences");
relm4::new_stateless_action!(pub(super) ShortcutsAction, WindowActionGroup, "show-help-overlay");
relm4::new_stateless_action!(AboutAction, WindowActionGroup, "about");

#[relm4::component(pub)]
impl SimpleComponent for App {
    type Init = ();
    type Input = AppMsg;
    type Output = ();
    type Widgets = AppWidgets;

    menu! {
        primary_menu: {
            section! {
                "_Preferences" => PreferencesAction,
                "_Keyboard" => ShortcutsAction,
                "_About Gnome Phone Companion" => AboutAction,
            }
        }
    }

    view! {
        main_window = adw::ApplicationWindow::new(&main_application()) {
            connect_close_request[sender] => move |_| {
                main_application().quit();
                gtk::Inhibit(true)
            },

            #[wrap(Some)]
            set_help_overlay: shortcuts = &gtk::Builder::from_resource(
                    "/org/bil4x4/gnomephonecompanion/gtk/help-overlay.ui"
                )
                .object::<gtk::ShortcutsWindow>("help_overlay")
                .unwrap() -> gtk::ShortcutsWindow {
                    set_transient_for: Some(&main_window),
                    set_application: Some(&main_application()),
            },
            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_hexpand: true,
                set_vexpand: true,

                #[name = "content_header"]
                adw::HeaderBar {
                    pack_start = &adw::ViewSwitcher {
                        set_policy: adw::ViewSwitcherPolicy::Wide,
                        set_stack: Some(&stack)
                    },

                    #[wrap(Some)]
                    set_title_widget = &adw::WindowTitle {
                        set_title: "Gnome Phone Companion",
                    },

                    pack_end = &gtk::MenuButton {
                        set_icon_name: "open-menu-symbolic",
                        set_menu_model: Some(&primary_menu),
                    },

                    pack_end = &gtk::SearchEntry,
                },
                #[name = "stack"]
                adw::ViewStack {
                    add_named: (model.contacts.widget(), Some("contacts_page")),
                    add_named: (model.photos.widget(), Some("photos_page")),
                    add_named: (model.files.widget(), Some("files_page"))
                },
            },


            add_css_class?: if PROFILE == "Devel" {
                    Some("devel")
                } else {
                    None
                },
        }
    }

    fn init(
        _init: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let about_dialog = AboutDialog::builder()
            .transient_for(root)
            .launch(())
            .detach();

        let contacts = Contacts::builder().launch(()).detach();

        let photos = Photos::builder().launch(()).detach();

        let files = Files::builder().launch(()).detach();

        let model = Self {
            about_dialog,
            contacts,
            photos,
            files,
        };
        let widgets = view_output!();
        let actions = RelmActionGroup::<WindowActionGroup>::new();

        let shortcuts_action = {
            let shortcuts = widgets.shortcuts.clone();
            RelmAction::<ShortcutsAction>::new_stateless(move |_| {
                shortcuts.present();
            })
        };

        let about_action = {
            let sender = model.about_dialog.sender().clone();
            RelmAction::<AboutAction>::new_stateless(move |_| {
                sender.send(());
            })
        };

        actions.add_action(&shortcuts_action);
        actions.add_action(&about_action);

        widgets
            .main_window
            .insert_action_group(WindowActionGroup::NAME, Some(&actions.into_action_group()));

        widgets.load_window_size();

        // Set Icons
        widgets
            .stack
            .page(&widgets.stack.child_by_name("contacts_page").unwrap())
            .set_icon_name(Some("avatar-default-symbolic"));
        widgets
            .stack
            .page(&widgets.stack.child_by_name("photos_page").unwrap())
            .set_icon_name(Some("emblem-photos-symbolic"));
        widgets
            .stack
            .page(&widgets.stack.child_by_name("files_page").unwrap())
            .set_icon_name(Some("system-file-manager-symbolic"));

        ComponentParts { model, widgets }
    }

    // fn update(&mut self, message: Self::Input, _sender: ComponentSender<Self>) {
    //     match message {
    //         AppMsg::Quit => main_application().quit(),
    //         _ => {}
    //     }
    // }

    fn shutdown(&mut self, widgets: &mut Self::Widgets, _output: relm4::Sender<Self::Output>) {
        widgets.save_window_size().unwrap();
    }
}

impl AppWidgets {
    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let settings = gio::Settings::new(APP_ID);
        let (width, height) = self.main_window.default_size();

        settings.set_int("window-width", width)?;
        settings.set_int("window-height", height)?;

        settings.set_boolean("is-maximized", self.main_window.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let settings = gio::Settings::new(APP_ID);

        let width = settings.int("window-width");
        let height = settings.int("window-height");
        let is_maximized = settings.boolean("is-maximized");

        self.main_window.set_default_size(width, height);

        if is_maximized {
            self.main_window.maximize();
        }
    }
}
