mod imp;

use gtk::prelude::*;
use relm4::{gtk, ComponentParts, ComponentSender, SimpleComponent};

use crate::pages::contacts::ContactsMsg;

#[derive(Debug)]
pub struct Contact {
    contact: String,
}

#[relm4::component(pub)]
impl SimpleComponent for Contact {
    type Init = ();
    type Input = ContactsMsg;
    type Output = ContactsMsg;
    type Widgets = ContactWidgets;

    view! {
        #[name = "root_widget"]
        gtk::Label {
            #[watch]
            set_label: &model.contact,
            set_hexpand: true,
            set_vexpand: true
        }
    }

    fn update(&mut self, msg: ContactsMsg, _sender: ComponentSender<Self>) {
        match msg {
            ContactsMsg::Selected(index) => {
                self.contact = String::from(format!("Contact {} Selected", index))
            }
            _ => {}
        }
    }

    fn init(_: (), root: &Self::Root, _sender: ComponentSender<Self>) -> ComponentParts<Self> {
        let model = Self {
            contact: String::from("Nothing selected..."),
        };
        let widgets = view_output!();

        Self::init_imp(&widgets);

        ComponentParts { model, widgets }
    }
}
