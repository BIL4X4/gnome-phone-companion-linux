mod imp;

use gtk::prelude::*;
use relm4::{gtk, ComponentParts, ComponentSender, SimpleComponent};

pub struct Files;

#[relm4::component(pub)]
impl SimpleComponent for Files {
    type Init = ();
    type Input = ();
    type Output = ();
    type Widgets = FilesWidgets;

    view! {
        #[name = "root_widget"]
        gtk::Label {
            set_label: "Files",
            set_hexpand: true,
            set_vexpand: true
        }
    }

    fn init(_: (), root: &Self::Root, _sender: ComponentSender<Self>) -> ComponentParts<Self> {
        let model = Self {};
        let widgets = view_output!();

        Self::init_imp(&widgets);

        ComponentParts { model, widgets }
    }
}
