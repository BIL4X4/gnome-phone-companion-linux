impl super::ContactList {
    pub(super) fn init_imp(&mut self, _widgets: &super::ContactListWidgets) {
        println!("ContactList::init_imp");

        // Test Data
        for n in 0..100 {
            self.list.guard().push_back((
                String::from(format!("Test User {}", n)),
                String::from(format!("0435 000 0{:0>2}", n)),
            ));
        }
    }
}
