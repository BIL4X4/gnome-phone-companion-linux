mod imp;

use crate::pages::contacts::ContactsMsg;
use relm4::{
    adw, adw::prelude::*, factory::FactoryVecDeque, gtk, prelude::*, ComponentParts,
    ComponentSender, SimpleComponent,
};

#[derive(Debug)]
pub struct ContactDetails {
    pub name: String,
    pub number: String,
}

#[relm4::factory(pub)]
impl FactoryComponent for ContactDetails {
    type Init = (String, String);
    type Input = ();
    type Output = ();
    type CommandOutput = ();
    type ParentInput = ContactsMsg;
    type ParentWidget = gtk::ListBox;
    type Widgets = ContactDetailsWidgets;

    view! {
        #[name = "root_widget"]
        adw::ActionRow::builder()
            .title(&self.name)
            .subtitle(&self.number)
            .build() {
                add_prefix = &adw::Avatar {
                    set_text: Some(&self.name),
                    set_show_initials: true,
                    set_size: 32
            }
        }
    }

    fn init_model(
        init_val: Self::Init,
        _index: &DynamicIndex,
        sender: FactoryComponentSender<Self>,
    ) -> Self {
        Self {
            name: init_val.0,
            number: init_val.1,
        }
    }
}

pub struct ContactList {
    list: FactoryVecDeque<ContactDetails>,
}

#[relm4::component(pub)]
impl SimpleComponent for ContactList {
    type Init = ();
    type Input = ContactsMsg;
    type Output = ContactsMsg;
    type Widgets = ContactListWidgets;

    view! {
        #[name = "root_widget"]
        gtk::ScrolledWindow {
            set_hscrollbar_policy: gtk::PolicyType::Never,
            set_min_content_height: 360,
            set_vexpand: true,

            #[local_ref]
            list_box -> gtk::ListBox {
                set_width_request: 220,
                connect_row_selected[sender] => move |_, row| {
                    if let Some(row) = row {
                        sender.output(ContactsMsg::Selected(row.index() as u32));
                    }
                }
            }
        }
    }

    fn update(&mut self, msg: ContactsMsg, _sender: ComponentSender<Self>) {
        match msg {
            ContactsMsg::Delete(index) => println!("Delete {}", index),
            _ => {}
        }
    }

    fn init(
        _: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let mut model = Self {
            list: FactoryVecDeque::new(gtk::ListBox::default(), sender.input_sender()),
        };

        let list_box = model.list.widget();

        let widgets = view_output!();

        Self::init_imp(&mut model, &widgets);

        ComponentParts { model, widgets }
    }
}
