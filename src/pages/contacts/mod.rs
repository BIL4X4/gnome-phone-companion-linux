mod contact_list;
mod imp;

use gtk::prelude::*;
use relm4::{
    adw, gtk, Component, ComponentController, ComponentParts, ComponentSender, Controller,
    SimpleComponent,
};

use crate::pages::{contact::Contact, messages::Messages, phone::Phone};
use contact_list::ContactList;

#[derive(Debug)]
pub enum ContactsMsg {
    Selected(u32),
    Delete(u32),
}

pub struct Contacts {
    contact_list: Controller<ContactList>,
    contact: Controller<Contact>,
    messages: Controller<Messages>,
    phone: Controller<Phone>,
}

#[relm4::component(pub)]
impl SimpleComponent for Contacts {
    type Init = ();
    type Input = ();
    type Output = ();
    type Widgets = ContactsWidgets;

    view! {
        #[name = "root_widget"]
        adw::Leaflet {
            connect_show[sender] => move |_| {
                println!("TEST...");
            },
            #[name = "contacts_container"]
            gtk::Box {
                set_vexpand: true
            },

            gtk::Separator {
                set_orientation: gtk::Orientation::Vertical
            },

            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,
                set_hexpand: true,
                set_vexpand: true,

                #[name = "stack"]
                adw::ViewStack {
                    add_named: (model.contact.widget(), Some("contact_page")),
                    add_named: (model.messages.widget(), Some("messages_page")),
                    add_named: (model.phone.widget(), Some("phone_page")),
                },

                gtk::Separator {
                    set_orientation: gtk::Orientation::Horizontal
                },

                adw::ViewSwitcher {
                    set_policy: adw::ViewSwitcherPolicy::Wide,
                    set_stack: Some(&stack),
                    set_height_request: 48,
                    set_margin_top: 16,
                    set_margin_bottom: 16,
                    set_hexpand: false,
                    set_halign: gtk::Align::Center
                }
            }
        }
    }

    fn init(_: (), root: &Self::Root, sender: ComponentSender<Self>) -> ComponentParts<Self> {
        let contact = Contact::builder().launch(()).detach();

        let contact_list = ContactList::builder()
            .launch(())
            .forward(contact.sender(), |msg| msg);

        let messages = Messages::builder().launch(()).detach();

        let phone = Phone::builder().launch(()).detach();

        let model = Self {
            contact_list,
            contact,
            messages,
            phone,
        };
        let widgets = view_output!();

        // Set Icons
        widgets
            .contacts_container
            .append(model.contact_list.widget());
        widgets
            .stack
            .page(&widgets.stack.child_by_name("contact_page").unwrap())
            .set_icon_name(Some("avatar-default-symbolic"));
        widgets
            .stack
            .page(&widgets.stack.child_by_name("messages_page").unwrap())
            .set_icon_name(Some("emblem-photos-symbolic"));
        widgets
            .stack
            .page(&widgets.stack.child_by_name("phone_page").unwrap())
            .set_icon_name(Some("system-file-manager-symbolic"));

        Self::init_imp(&widgets);

        ComponentParts { model, widgets }
    }
}
